<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;
use App\User;

class CastController extends Controller
{
    public function index(){
      $data=[

        'casts' => Cast::all()
      ];

      return view('Cast', $data);
    }

    public function postCast()
    {

      // if(empty($_POST['content'])){
      //   die('You must enter a podcast');
      // }

      $this->validate(request(), [
        'content' => 'required',
        'description' =>'required'
      ]);

      $cast= new Cast();
      $cast->content = $_POST['content'];
      $cast->description = $_POST['description'];
      $cast->user_id = request()->user()->id;
      $cast->save();

      return redirect('/');

    }

    public function user(){
      $userId = $_GET['id'];
      $user = User::find($userId);
      if(is_null($user)){
        die('This user does not exist');
      }
      $casts = Cast::where('user_id', $userId)->get();
      $data =[
        'user' => $user,
        'casts' => $casts
      ];
      return view('user', $data);
    }
}
