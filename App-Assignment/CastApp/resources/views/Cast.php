<header>
  <?php if(Auth::check()) {?>
  Hello, <?php echo request()->user()->name ?>
  <form action="logout" method="post">
    <?php echo csrf_field() ?>
    <input type="submit" name="submit" value="Logout">
  </form>
</header>

<?php } else { ?>
<a href="login">Login</a>
<?php } ?>
<h1>Podcast</h1>

<?php if($errors) {?>
  <?php foreach($errors->all() as $message) {?>
      <?php echo $message ?>
  <?php } ?>
<?php } ?>

<ul>
  <?php foreach ($casts as $cast) { ?>
    <li>
       <?php echo $cast->content ?>
       <?php echo $cast->description ?>
      <a href="user?id= <?php echo $cast->getUser()->id ?>">
        (<?php echo $cast->getUser()->name ?>)
      </a>

    </li>
  <?php  } ?>
</ul>

<?php if(Auth::check()){ ?>
<form action="" method="post">
  <?php echo csrf_field() ?>
  Title:<input type="text" name="content" value="">
  Description: <input type="text" name="description" value="">
  <input type="submit" name="submit" value="Submit">
</form>
<?php } ?>
