<header>
  Hello, <?php echo request()->user()->name ?>
  <form action="logout" method="post">
    <?php echo csrf_field() ?>
    <input type="submit" name="submit" value="Logout">
  </form>
</header>


<a href=".">Home</a>

<h1>User: <?php echo $user->name?></h1>

<h2><?php echo $user->name ?>'s  Podcasts</h2>

<ul>
  <?php foreach ($casts as $cast){ ?>
      <li>
         <?php echo $cast->content ?> -
         <?php echo $cast->created_at->format('d M Y H:i:s a') ?> -
         (<?php echo $cast->getUser()->name ?>)
       </li>

  <?php } ?>
</ul>
